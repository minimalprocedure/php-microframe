<?php
// -------------------------------------------------------------------------------------
$EnableGZipEncoding = true;
// -------------------------------------------------------------------------------------

// Helper function to detect if GZip is supported by client!
// If not supported the tricks are pointless
function acceptsGZip(){
    $accept = str_replace(" ","",
        strtolower($_SERVER['HTTP_ACCEPT_ENCODING'])
    );
    $accept = explode(",",$accept);
    return in_array("gzip",$accept);
}
// -------------------------------------------------------------------------------------
function playWithHtml($OutputHtml){
    // This will mess up HTML code like my site has done!
    // View the source to understand! All ENTERs are removed.
    // If your site has PREformated code this will break it!
    // Use regexp to find it and save it and place it back ...
    // or just uncomment the next line to keep enters
    // return $OutputHtml;
    return preg_replace("/\s+/"," ",$OutputHtml);
}
// -------------------------------------------------------------------------------------
function obOutputHandler($OutputHtml){
    global $EnableGZipEncoding;
    //-- Play with HTML before output
    $OutputHtml = playWithHtml($OutputHtml);
    //-- If GZIP not supported compression is pointless.
    // If headers were sent we can not signal GZIP encoding as
    // we will mess it all up so better drop it here!
    // If you disable GZip encoding to use plain output buffering we stop here too!
    if(!acceptsGZip() || headers_sent() || !$EnableGZipEncoding) return $OutputHtml;
    //-- We signal GZIP compression and dump encoded data
    header("Content-Encoding: gzip");
    return gzencode($OutputHtml);
}
// This code has to be before any output from your site!
// If output exists uncompressed HTML will be delivered!
ob_start("obOutputHandler");
?>