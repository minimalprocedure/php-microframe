<?php

function build_header_callback($template, $params, $raw_processing = NULL){
    global $media_path;
    global $views_path;
    global $languages;

    $TEMPLATE = "";

    $processor = PROCESSOR;

    $VARS["WEBSITE_TITLE"] = WEBSITE_TITLE;
    $VARS["MEDIA_HEADER"] = media_insert($views_path.$media_path.$params);

    $VARS["LANGUAGES"] = "";
    foreach($languages as $code => $text ){
        $VARS["LANGUAGES"] .= "<li><a href=\"$processor?lang=$code\">$text</a></li>";
        build_docu(BASE_VIEWS_PATH.$code."/");
    }
    //include $template;

    if (!$raw_processing){
        include $template;
    } else {
        $TEMPLATE = $template;
    }

    return $TEMPLATE;
}

function build_menu_callback($template, $params, $raw_processing = NULL){

    $processor = PROCESSOR;

    if($params['root']){
        $views_path = BASE_VIEWS_PATH.$_SESSION['AREAS'][0]."/";
    }else{
        global $views_path;
    }

    require(path_norm($views_path.'navigation.php'));

    switch ($params['type']){

        case 'menu':
            $VARS["MENU_LIST"] ='';
            $menu = $navigation['menu'];
            if (count($menu > 0)){
                $level = $menu['level'];
                foreach($menu as $index => $value){
                    if($value['visible']){
                        if($index < 9){
                            $i = '0'.$index;
                        }

                        $caption = $value['caption'];

                        if($value['url']){
                            $url = $value['url'];
                            if ($value['target']){
                                $target = $value['target'];
                                $VARS["MENU_LIST"] .= "<li><a href=\"$url\" target=\"$target\">$caption</a></li>";
                            } else {
                                $VARS["MENU_LIST"] .= "<li><a href=\"$url\">$caption</a></li>";
                            }
                        } else {
                            if(BUILD_MENU_BY_INDEX){
                                $VARS["MENU_LIST"] .= "<li><a href=\"$processor?area=area_$i&amp;level=$level\">$caption</a></li>";
                            } else {
                                //$caption_path = rem_space_to_lower($caption);
                                $caption_path = rem_space_to_lower($value['folder']);
                                $VARS["MENU_LIST"] .= "<li><a href=\"$processor?area=$caption_path&amp;level=$level\">$caption</a></li>";
                            }
                        }
                    }
                }
                include $template;
                return $TEMPLATE;
            }

        case 'links':
            $TEMPLATE = "";

            $TEMPLATE = "";
            if (!$raw_processing){
                include $template;
            } else {
                $TEMPLATE = $template;
            }

            $links = $navigation['links'];
            if (count($links > 0)){
                foreach($links as $index => $value){
                    if($index < 9){
                        $i = '0'.$index;
                    }

                    $caption = $value['caption'];

                    if($value['url']){
                        $url = $value['url'];
                        if ($value['target']){
                            $target = $value['target'];
                            $link = "<a href=\"$url\" target=\"$target\">$caption</a>";
                        } else {
                            $link = "<a href=\"$url\">$caption</a>";
                        }
                        if($value['visible']){
                            $TEMPLATE = str_replace("<(".$links[$index]['caption'].")>", $link,  $TEMPLATE);
                        } else {
                            $TEMPLATE = str_replace("<(".$links[$index]['caption'].")>", "",  $TEMPLATE) ;
                        }
                    } else {
                        $level = $value['level'];
                        if(BUILD_MENU_BY_INDEX){
                            if($params['link_no_close']){
                                $link = "<a href=\"$processor?area=area_$i&amp;level=$level\">";
                            } else {
                                $link = "<a href=\"$processor?area=area_$i&amp;level=$level\">$caption</a>";
                            }
                        } else {
                            $caption_path = rem_space_to_lower($value['folder']);
                            if($params['link_no_close']){
                                $link = "<a href=\"$processor?area=$caption_path&amp;level=$level\">";
                            } else {
                                $link = "<a href=\"$processor?area=$caption_path&amp;level=$level\">$caption</a>";
                            }
                        }
                        if($value['visible']){
                            if($params['link_no_close']){
                                $TEMPLATE = str_replace("<(".$links[$index]['caption']."-noclose)>", $link,  $TEMPLATE);
                            } else {
                                $TEMPLATE = str_replace("<(".$links[$index]['caption'].")>", $link,  $TEMPLATE);
                            }
                        } else {
                            if($params['link_no_close']){
                                $TEMPLATE = str_replace("<(".$links[$index]['caption']."-noclose)>", "",  $TEMPLATE) ;
                            } else {
                                $TEMPLATE = str_replace("<(".$links[$index]['caption'].")>", "",  $TEMPLATE) ;
                            }
                        }

                    }
                }
            }
            return $TEMPLATE;

    }
}

function debug_callback($template, $params, $raw_processing = NULL){
    ob_start();
    print_r($params, false);
    $VARS[$DEBUG] =  ob_get_contents();
    ob_end_clean();
    include $template;
    return $TEMPLATE;
}

function resolve_media_path_callback($template, $params, $raw_processing = NULL){

    if($params['root']){
        $views_path = BASE_VIEWS_PATH.$_SESSION['AREAS'][0]."/";
    }else{
        global $views_path;
    }

    $TEMPLATE = "";
    if (!$raw_processing){
        include $template;
    } else {
        $TEMPLATE = $template;
    }
    $template = str_replace("{[MEDIA_PATH]}", "$views_path",  $TEMPLATE) ;
    return $template;
}

function resolve_images_path_callback($template, $params, $raw_processing = NULL){

    if($params['root']){
        $views_path = BASE_VIEWS_PATH.$_SESSION['AREAS'][0]."/";
    }else{
        global $views_path;
    }

    $TEMPLATE = "";
    if (!$raw_processing){
        include $template;
    } else {
        $TEMPLATE = $template;
    }
    $template = str_replace("{[IMAGES_PATH]}", "$views_path",  $TEMPLATE) ;
    return $template;
}

function substitution($template, $params, $raw_processing = NULL){

    $TEMPLATE = "";
    if (!$raw_processing){
        include $template;
    } else {
        $TEMPLATE = $template;
    }
    $server_name = "http://".$_SERVER['SERVER_NAME'];
    $url = $server_name.$_SERVER["REQUEST_URI"];
    $url = parse_url($url);

    $server_root = str_replace(PROCESSOR, "", $server_name.$url["path"]);

    $subst = array(
        "{[DOCUMENT_ROOT]}",
        "{[SERVER_NAME]}",
        "{[WEBSITE_TITLE]}",
        "{[WEBSITE_TOOLS]}",
        "{[WEBSITE_VERSION]}"
        );
   $text = array(
        $server_root,
        $server_name,
        WEBSITE_TITLE,
        $server_root.BASE_WEBSITE_TOOLS_PATH,
        getPortalVersion()
        );

    if(is_array($params) && count($params) == 2) {
        $subst = array_merge($subst, $params[0]);
        $text = array_merge($text, $params[1]);

    }

    $template = str_replace($subst, $text,  $TEMPLATE);
    return $template;
}


/* Produces browser-safe strings while preserving HTML tags.
 * This function takes the string "ISTRING" as an argument and returns
 * the string "OSTRING". All special characters from "ISTRING" are
 * converted to browser-safe HTML codes using htmlentities(), and HTML
 * tags are left intact and functional.
 */

// Contributed Nov 2000 by Jason Galvin: <www.jasongalvin.com
// mod: nissl
function guardHTML($istring) {
    $stringarr = preg_split('//', $istring);
    $stringlen = count($stringarr);
    for($i=0; $i<$stringlen; ++$i){
        if($stringarr[$i] == "<"){
            while($i<$stringlen){
                $HTMLtag .= $stringarr[$i];
                ++$i;
                if($stringarr[($i-1)] == ">") break;
            }
            --$i;
        } else {
            while($stringarr[$i] != "<" && $i<$stringlen){
                $nonHTML .= $stringarr[$i];
                ++$i;
            }
            --$i;
        }
        $ostring .= $HTMLtag;
        $HTMLtag = "";
        $ostring .= htmlentities($nonHTML, ENT_NOQUOTES, "UTF-8");
        $nonHTML = "";
    }
    return $ostring;
}

function prepare_output($template, $params, $raw_processing = NULL){

    $TEMPLATE = "";
    if (!$raw_processing){
        include $template;
    } else {
        $TEMPLATE = $template;
    }

    $replace = Array("\n", "\r");
    $with = Array(" ", "");

    $template = str_replace($replace, $with,  $template);
    $template = str_replace("  ", "",  $template);
    if ($params['convert']){
        //$template = htmlentities($template, ENT_QUOTES, "UTF-8");
        $template = guardHTML($template);
    }

    return trim($template);
}

function test1_callback($template, $params, $raw_processing = NULL){

    $template = str_replace("{[MEDIA_HEADER]}", "$params",  $template) ;
    return $template;
}

function test2_callback($template, $params, $raw_processing = NULL){

    $template = str_replace("{[MEDIA_HEADER2]}", "$params",  $template) ;
    return $template;
}


function content_processor($template, $params, $raw_processing = NULL){
    return false;
}

?>
