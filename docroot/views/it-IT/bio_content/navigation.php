<?php

$navigation = array(

    "menu" => array(
        "level" => 0,
        array(
            "caption" => "Home",
            "folder" => "home_content",
            "visible" => true,
        )
    ),

    "links" => array(
        array(
            "caption" => "link_1",
            "folder" => "link_1_content",
            "visible" => true,
            "level" => 2
        )
    )
);

?>