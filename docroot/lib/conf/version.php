<?php

function getPortalVersion(){
    $VERSION = 'x.x.x.x';
    if (file_exists('version')) {
        $VERSION = file_get_contents('version');
    }
    return $VERSION;
}

define('VERSION', getPortalVersion())

?>