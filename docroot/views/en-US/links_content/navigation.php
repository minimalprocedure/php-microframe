<?php

$navigation = array(

    "menu" => array(
        "level" => 0,
        array(
            "caption" => "Home",
            "folder" => "home_content",
            "visible" => true,
        )
    ),

    "links" => array(
        array(
            "caption" => "www.zoemagazine.net",
            "visible" => true,
            "url" => "http://www.zoemagazine.net"
        ),
        array(
            "caption" => "www.rbcasting.com",
            "visible" => true,
            "url" => "http://www.rbcasting.com/site/vanessagalipoli.rb"
        )
    )
);

?>