<?php

function rem_space_to_lower($caption){
    return strtolower(str_replace(" ", "_", $caption));
}

function path_norm($path){
    $path = str_replace("//", "/", $path);
    return $path;
}

function mkdir_r($dirName, $rights = 0777){
    $dirs = explode('/', $dirName);
    $dir='';
    foreach ($dirs as $part) {
        $dir.=$part.'/';
        if (!is_dir($dir) && strlen($dir)>0)
        mkdir($dir, $rights);
    }
}

function media_insert($media_file){

    $swf_media=
    <<<MEDIA
<object
    type="application/x-shockwave-flash"
    data="%MEDIA%"
    width="738"
    height="240">
    <param name="movie" value="%MEDIA%" />
    <param name="loop" value="false" />
    <param name="quality" value="high" />
    <param name="menu" value="false" />
    <param name="bgcolor" value="4F4B4A" />
</object>
MEDIA;

    $image_media=
    <<<MEDIA
<img src="%MEDIA_HEADER%" alt=""></img>
MEDIA;

    $file_type = substr(strrchr($media_file, "."), 1);
    //$media_file = $views_path.$media_path.$media_file;

    switch ($file_type) {
        case "png":
            $TEMPLATE = str_replace("%MEDIA%", $media_file, $image_media);
            break;
        case "jpg":
            $TEMPLATE = str_replace("%MEDIA%", $media_file, $image_media);
            break;
        case "swf":
            $TEMPLATE = str_replace("%MEDIA%", $media_file, $swf_media);
            break;
    }
    return $TEMPLATE;
}

function build_docu($views_path){
    $template_base_string=
    <<<BASE
<?php
\$TEMPLATE=
<<<TEMPLATE

    <div id="content_box">
        <div id="content">
            content area: %area%
        </div>
    </div>

TEMPLATE;
?>
BASE;

    $navigation_base_string=
    <<<BASE
<?php

\$navigation = array(

    "menu" => array(
        "level" => 0,
        array(
            "caption" => "Home",
            "folder" => "home_content",
            "visible" => true,
        )
    ),

    "links" => array(
        array(
            "caption" => "link_1",
            "folder" => "link_1_content",
            "visible" => true,
            "level" => 0
        )
    )
);

?>
BASE;

    if(MODE_BUILD){
        if(!file_exists($views_path)) {
            mkdir_r($views_path);
            mkdir_r($views_path.BASE_MEDIA_PATH);
        }
        if(!file_exists($views_path.TEMPLATE_BASEFILE)) {
            $hf = fopen($views_path.TEMPLATE_BASEFILE, "x+");
            $template_base_string = str_replace("%area%", $views_path, $template_base_string);
            fwrite($hf, $template_base_string);
            fclose($hf);
        }
        if(!file_exists($views_path.NAVIGATION_BASEFILE)) {
            $hf = fopen($views_path.NAVIGATION_BASEFILE, "x+");
            fwrite($hf, $navigation_base_string);
            fclose($hf);
        }
    }
}

function build_all_structure(){

    global $BASE_PROJECT_DIRS;

    $header_base=
    <<<BASE

/*************************************************
    (c) 2003-2006 by LINee multimediaagency -
    www.linee.it - info@linee.it
    All rights reserved.
*************************************************/

BASE;

    $debug_style=
    <<<BASE

div.debug_area{
    position: absolute;
    background-color: rgb(200,200,200);
    border: 1px solid rgb(100,100,100);
    width: 100%;
    bottom:0px;
}

pre.pre_debug_area{
    border-bottom: 1px solid rgb(100,100,100);
}

BASE;

    $debug_template =
    <<<BASE

<?php
\$TEMPLATE=
<<<TEMPLATE

    <pre class="pre_debug_area">
        {\$VARS[\$DEBUG]}
    </pre>
    <br />

TEMPLATE;
?>

BASE;

    $footer_template =
    <<<BASE

<?php
\$TEMPLATE=
<<<TEMPLATE

    <div id="footer_box">
        <div id="footer_content">
            [CORPORATE] <a   class="link_01" href="mailto:info@mail.com">i[CORPORATE EMAIL]</a>
        </div>
    </div>

</div>
</body>
</html>

TEMPLATE;
?>

BASE;

    $header_template =
    <<<BASE

<?php
\$TEMPLATE=
<<<TEMPLATE

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
    <title>{\$VARS["WEBSITE_TITLE"]}</title>
    <meta http-equiv="content-type" content="text/html; charset=ISO-8859-1" />
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <meta name="author" content="Linee Multimediaagency - www.linee.it" />
    <meta name="category" content="" />
    <meta http-equiv="revisit-after" content="15 days" />
    <link rel="shortcut icon" href="ico/favicon.ico" />
    <link rel="stylesheet" type="text/css" media="screen" href="css/styles.css" />
</head>
<body>
<div id="main_content">
    <div id="header_box">

        <div id="header_block_top_box">
            <h1 id="website_title">[TITLE]</h1>
            <ul id="language">
                {\$VARS["LANGUAGES"]}
            </ul>
        </div>

        <div id="header_media_box">
            {\$VARS["MEDIA_HEADER"]}
        </div>

    </div>

TEMPLATE;
?>

BASE;

    $menu_template =
    <<<BASE

<?php
\$TEMPLATE=
<<<TEMPLATE

    <div id="navigation_menu">
        <ul id="main_menu">
            {\$VARS["MENU_LIST"]}
        </ul>
    </div>

TEMPLATE;
?>

BASE;

    $lang_template =
    <<<BASE

<?php

    \$languages = array(
        "it-IT" => "italiano",
        "en-US" => "english"
    )

?>

BASE;

    if(!MODE_BUILD_ALL_STRUCTURE)
    return false;

    $styles_open = false;
    foreach($BASE_PROJECT_DIRS as $base => $files ){
        if(!file_exists($base)) {
            mkdir_r($base);
            switch ($base) {

                case "css":
                    if(!file_exists($base."/styles.css")){
                        $hfs = fopen($base."/styles.css", "x+");
                        fwrite($hfs, $header_base);
                        fwrite($hfs, "@import \"debug.css\";");
                        $styles_open = true;
                    }
                    if(!file_exists($base."/debug.css")){
                        $hfd = fopen($base."/debug.css", "x+");
                        fwrite($hfd, $header_base);
                        fwrite($hfd, $debug_style);
                        fclose($hfd);
                    }

                    foreach($files as $file){
                        if(!file_exists($base."/".$file)){
                            $hf = fopen($base."/".$file, "x+");
                            fwrite($hf, $header_base);
                            fclose($hf);
                        }
                        if($styles_open)
                        fwrite($hfs, "@import \"$file\";");
                    }
                    if($styles_open)
                    fclose($hfs);
                    break;

                case "views":
                    if(!file_exists($base."/debug".TEMPLATE_EXT)){
                        $hf = fopen($base."/debug".TEMPLATE_EXT, "x+");
                        fwrite($hf, $debug_template);
                        fclose($hf);
                    }
                    if(!file_exists($base."/footer".TEMPLATE_EXT)){
                        $hf = fopen($base."/footer".TEMPLATE_EXT, "x+");
                        fwrite($hf, $footer_template);
                        fclose($hf);
                    }
                    if(!file_exists($base."/header".TEMPLATE_EXT)){
                        $hf = fopen($base."/header".TEMPLATE_EXT, "x+");
                        fwrite($hf, $header_template);
                        fclose($hf);
                    }
                    if(!file_exists($base."/menu".TEMPLATE_EXT)){
                        $hf = fopen($base."/menu".TEMPLATE_EXT, "x+");
                        fwrite($hf, $menu_template);
                        fclose($hf);
                    }
                    if(!file_exists($base."/languages.php")){
                        $hf = fopen($base."/languages.php", "x+");
                        fwrite($hf, $lang_template);
                        fclose($hf);
                    }
                    // build files list
                    foreach($files as $file){
                        if(!file_exists($base."/".$file)){
                            $hf = fopen($base."/".$file, "x+");
                            fwrite($hf, $header_base);
                            fclose($hf);
                        }
                    }
                    break;

                default:
                    foreach($files as $file){
                        if(!file_exists($base."/".$file)){
                            $hf = fopen($base."/".$file, "x+");
                            fwrite($hf, $header_base);
                            fclose($hf);
                        }
                    }
                    break;
            }



        }
    }
}

?>
