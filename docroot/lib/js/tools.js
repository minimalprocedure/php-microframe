function load_gallery(url, gallery){
    var obj = $("img-thumbs")
    new Ajax.Updater(
        { success: obj },
        url,
        {
            method: 'get',
            parameters: "gallery=" + gallery,
            onComplete: function(){
               initLightbox()
            }
        }
    )
}

function change_header(photo){
    var header_media = $("header_media_box");
    header_media.innerHTML = "";
    header_media.style.height = "100px";
    header_media.innerHTML =
        '<img src="' + photo + '" alt="showreel" />';
}

function play_image_big(el){
    var image = el.src.replace("thumbs/", "normals/")
    var content = $("content")
    content.style.display = 'none'

    var image_big_preview = $("content-image-preview")

    if (!image_big_preview){
        $("content_box").innerHTML += ' '
            + '<div id="content-image-preview">'
            + '<div class="close-button" onclick="close_image_preview()">X</div>'
            + '<div class="image-preview">'
            + '<img id="img-image-preview" src="' + image + '" alt=""'
            + '</div>'
            + '</div>'
        image_big_preview = $("content-image-preview")
        image_big_preview.style.display  = 'none'
        }
        $("img-image-preview").src = image
        image_big_preview.style.display  = 'block'
}

function close_image_preview(){
    $("content-image-preview").style.display = 'none'
    $("content").style.display = 'block'
}

function play_flv(video){
    var el = $("media-box")
    el.innerHTML = ' '
    + '<object id="showreel-flash" classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,0,0" width="273" height="204">'
    + '<PARAM NAME=allowFlashAutoInstall VALUE=true>'
    + '<param id="showreel-flash-param_url" name=Flashvars value="url=' + video + '">'
    + '<param name="allowScriptAccess" value="sameDomain" />'
    + '<param name="movie" value="media/320x240.swf" />'
    + '<param name="quality" value="high" />'
    + '<param name="bgcolor" value="#030303" />'
    + '<embed id="showreel-flash-embed" src="media/320x240.swf" swLiveConnect="true" Flashvars="url=' + video + '" quality="high" bgcolor="#030303" width="273" height="204" name="320x240" align="middle" allowScriptAccess="sameDomain" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer" />'
    + '</object>'
}

function play_img(image){

    var el = $("media-box")
    el.innerHTML = ' '
        + '<img src="' + image + '" alt="">'
}
