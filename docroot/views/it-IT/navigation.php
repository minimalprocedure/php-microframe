<?php

$navigation = array(

    "menu" => array(
        "level" => 1,
        array(
            "caption" => "bio",
            "folder" => "bio_content",
            "visible" => true,
        ),
        array(
            "caption" => "work",
            "folder" => "work_content",
            "visible" => true
        ),
        array(
            "caption" => "photo",
            "folder" => "photo_content",
            "visible" => true
        ),
        array(
            "caption" => "press",
            "folder" => "press_content",
            "visible" => true
        ),
        array(
            "caption" => "showreel",
            "folder" => "showreel_content",
            "visible" => true
        ),
        array(
            "caption" => "links",
            "folder" => "links_content",
            "visible" => true
        ),
        array(
            "caption" => "mail",
            "folder" => "mail_content",
            "visible" => true,
            "url" => "mailto:info@vanessagalipoli.it?subject=[REQ:INFO]%20insert%20subject%20here..."
        )
    ),

    "links" => array()
);


?>

