<?php
/****************************************
 Configuration
 ****************************************/

/*
 * Processor
 */
define('PROCESSOR', 'processor.php');

/*
 * Operation settings
 */
define('MODE_BUILD_ALL_STRUCTURE', false);
//define('BUILD_MENU_BY_INDEX', false);
define('BUILD_MENU_BY_INDEX', false);
define('MODE_DEBUG', false);
define('MODE_BUILD', true);

define('NO_STRIP_OUTPUT', true);

define('USE_DEFAULT_LANG_CONTENT_VALUE', true);
define('DEFAULT_LANG', 'it-IT');
define('DEFAULT_CONTENT', 'bio_content');
define('WEBSITE_TITLE', 'Vanessa Galipoli');
/*
 *   Base directories path
 */
define('BASE_PUBLIC', './');
define('BASE_VIEWS_PATH', 'views/');
define('BASE_CONTENT_PATH', 'content/');
define('BASE_MEDIA_PATH', 'media/');
define('BASE_ENGINE_CONF_PATH', 'conf/');
define('BASE_WEBSITE_TOOLS_PATH', 'lib/tools/');

/*
 Template file extension
 */
define('TEMPLATE_BASENAME', 'content');
define('TEMPLATE_EXT', '.php-tmpl');
define('TEMPLATE_BASEFILE', TEMPLATE_BASENAME.TEMPLATE_EXT);
define('NAVIGATION_BASEFILE', 'navigation.php');


/*
 * don't touch!!!!
 */
$views_path = BASE_VIEWS_PATH;
$content_path = BASE_CONTENT_PATH;
$media_path = BASE_MEDIA_PATH;
$engine_conf_path = BASE_ENGINE_CONF_PATH;
$website_tools = BASE_WEBSITE_TOOLS_PATH;


?>
