<?php
/* COMPRESSION */
require_once "lib/engine/compressor.php";
require_once "lib/conf/version.php";

/* SESSION */
session_start();

/* REQUIRE */
require_once "lib/conf/configuration_website.php";
require_once "lib/conf/configuration_engine.php";
require_once "lib/conf/subst_global.php";
require_once "lib/conf/languages.php";
require_once "lib/engine/tools.php";
require_once "lib/engine/callbacks.php";
require_once "lib/engine/engine.php";

get_lang();

if(MODE_BUILD_ALL_STRUCTURE){
    require "lib/conf/base_project_structure.php";
    build_all_structure();
}

get_area();
current_path();

$WEBSITE = "";

$WEBSITE .= get_view('html-head', true, 'substitution');

//echo get_view('header', true , 'swf_or_jpg_callback', 'header.swf');

$WEBSITE .=
    get_view(
        'header',
        true ,
        array('build_header_callback', 'substitution'),
        array('header.swf', array(array("pippo"), array("pluto")) ),
        "raw-not-first-proc"
        );

$WEBSITE .=
    get_view(
        'menu',
        true,
        'build_menu_callback',
        array('root' => true, 'type' => 'menu')
        );

// template statici
//echo get_view('content', false );

//echo get_view('content', false , 'build_menu_callback', array('root' => false, 'type' => 'links'));

/*

*/
if (NO_STRIP_OUTPUT || MODE_DEBUG){
    $WEBSITE .=
        get_view(
            'content',
            false,
            array(
                    'substitution',
                    'resolve_images_path_callback',
                    'build_menu_callback',
                    'build_menu_callback'
                    ),
            array(
                    array(),
                    array('root' => false),
                    array('root' => false, 'type' => 'links'),
                    array('root' => false, 'type' => 'links', 'link_no_close' => true)
                    )
                    );
} else {
    $WEBSITE .=
        get_view(
            'content',
            false,
            array(
                    'substitution',
                    'resolve_images_path_callback',
                    'build_menu_callback',
                    'build_menu_callback',
                    'prepare_output'
                    ),
            array(
                    array(),
                    array('root' => false),
                    array('root' => false, 'type' => 'links'),
                    array('root' => false, 'type' => 'links', 'link_no_close' => true),
                    array('convert' => true)
                    )
                    );
}
/*
 echo get_view(
 'content',
 false ,
 array('test1_callback', 'test2_callback'),
 array('pippo', 'pluto')
 );
 */
$WEBSITE .=
    get_view('footer', true, 'substitution');

/* ---------------------------------------------------------------------------- */
if(MODE_DEBUG){
    $WEBSITE .= "<div class=\"debug_area\">";

    $WEBSITE .= get_view('debug', true, 'debug_callback', "views_path: " . $views_path );
    $WEBSITE .= get_view('debug', true, 'debug_callback', $_SESSION['AREAS']);

    $WEBSITE .= "</div>";
}
/* ---------------------------------------------------------------------------- */

$WEBSITE .=
    get_view('html-tail', true, 'substitution');

echo get_view(
        $WEBSITE,
        true,
        'substitution',
        $SUBST_GLOBAL,
        true
    );
?>
