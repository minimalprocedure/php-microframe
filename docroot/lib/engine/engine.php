<?php

function get_lang(){

    global $languages;

    if(!isset($_SESSION['AREAS'])){

        if(USE_DEFAULT_LANG_CONTENT_VALUE){
            $default_lang = DEFAULT_LANG;
            $default_content = DEFAULT_CONTENT;
        } else {
            $lang_codes =  array_keys($languages);
            if(count($lang_codes) > 0){
                $default_lang = $lang_codes[0];
            } else {
                $default_lang = DEFAULT_LANG;
            }

            require BASE_VIEWS_PATH.$default_lang."/navigation.php";
            if(BUILD_MENU_BY_INDEX){
                $default_content = "area_00";
            } else {
                $default_content = rem_space_to_lower($navigation["menu"][0]["caption"]);
            }
        }

        $_SESSION['AREAS'] = array($default_lang);
        $_SESSION['AREAS'][1] = $default_content;
    } else {
        if(isset($_GET["lang"])){
            $_SESSION['AREAS'][0] = $_GET["lang"];
            //$_SESSION['AREAS'][1] = DEFAULT_CONTENT;
        }
    }
}

function current_path(){
    global $views_path;

    $areas = $_SESSION['AREAS'];
    $views_path = BASE_VIEWS_PATH;
    foreach($areas as $area){
        $views_path .= "$area/";
    }
    // build docus
    build_docu($views_path);
}

function check_if_lang_get(){
    global $languages;
    array_key_exists($_GET['area'], $languages);
}

function get_area(){
    if(isset($_GET['level']) and isset($_GET['area'])){
        if((count($_SESSION['AREAS'])) < $_GET['level']){
            $paths = split("/", $_GET['area']);
            foreach($paths as $path){
                array_push($_SESSION['AREAS'], $path);
            }
        } else {
            if(strpos($_GET['area'], "/")){
                $paths = split("/", $_GET['area']);
                $last = $paths[count($paths) - 1];
                $_SESSION['AREAS'][$_GET['level']] = $last;
            } else {
                $_SESSION['AREAS'][$_GET['level']] = $_GET['area'];
            }
        }
        if(!check_if_lang_get()){
            foreach($_SESSION['AREAS'] as $index => $value){
                if($index > $_GET['level']){
                    unset($_SESSION['AREAS'][$index]);
                }
            }
        }
    }
}


function get_view($view_file, $root = true, $template_processor = NULL, $template_processor_params = NULL, $raw_processing = NULL){

    if($root){
        $views_path = BASE_VIEWS_PATH;
    }else{
        global $views_path;
    }

    if($template_processor && ( is_array($template_processor) || is_callable($template_processor) )){
        if(is_array($template_processor)){

            $raw = true;
            if($raw_processing == "raw-not-first-proc"){
                $raw = false;
            }

            if($raw){
                $result = "";
                include $views_path.$view_file.TEMPLATE_EXT;
                $result = $TEMPLATE;
            } else {
                $result = $views_path.$view_file.TEMPLATE_EXT;
            }

            foreach($template_processor as $params => $tproc){
                $result = $tproc($result,  $template_processor_params[$params], $raw);
                $raw = true;
            }
            return $result;

        } else {
            if(is_callable($template_processor)){

                if($raw_processing == "raw-text"){
                    return $template_processor($view_file,
                        $template_processor_params, true);
                } else {
                    return $template_processor($views_path.$view_file.TEMPLATE_EXT,
                        $template_processor_params, $raw_processing);
                }

            }
        }
    } else {
        include $views_path.$view_file.TEMPLATE_EXT;
        return $TEMPLATE;
    }
}


?>
