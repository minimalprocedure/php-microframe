<?php

$navigation = array(

    "menu" => array(),

    "links" => array(
        array(
            "caption" => "televisione",
            "folder" => "televisione_content",
            "visible" => true,
            "level" => 2
        ),
        array(
            "caption" => "cinema",
            "folder" => "cinema_content",
            "visible" => true,
            "level" => 2
        ),
        array(
            "caption" => "cortometraggi",
            "folder" => "cortometraggi_content",
            "visible" => true,
            "level" => 2
        ),
        array(
            "caption" => "teatro",
            "folder" => "teatro_content",
            "visible" => true,
            "level" => 2
        ),
        array(
            "caption" => "spettacoli",
            "folder" => "spettacoli_content",
            "visible" => true,
            "level" => 2
        ),
        array(
            "caption" => "pubblicità",
            "folder" => "pubblicita_content",
            "visible" => true,
            "level" => 2
        )
    )
);

?>